package inf319ng.assoccp;

import java.util.ArrayList;

public class Companhia {
	
	private ArrayList<Pessoa> pessoas;

	public Companhia() {
		this.pessoas = new ArrayList<Pessoa>();
	}

	public double custoTotal() {
		double custoTotal = 0.0;
		
		for (Pessoa pessoa : pessoas) {
			custoTotal += pessoa.getSalario();
		}
		
		return custoTotal;
	}

	public void emprega(Pessoa p, double salario) {
		this.pessoas.add(p);
		p.setSalario(salario);
	}

	public void demite(Pessoa p) {
		this.pessoas.remove(p);
		p.setSalario(0.0);
	}

}
