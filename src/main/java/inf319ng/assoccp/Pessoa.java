package inf319ng.assoccp;

public class Pessoa {

	private double salario;
	
	public Pessoa() {
		this.salario = 0.0;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

}
