INF319 - Projeto e Implementação Orientados a Objetos

Aluno: Guilherme Kayo Shida

Comentário:

- A classe Companhia tem uma relação unidirecional para com a classe Pessoa,
  através do atributo pessoas;
- O atributo pessoas é uma lista com todos as pessoas;
- A classe Companhia contrata e demite uma Pessoa.